=====
PtvPy
=====

`Source code <https://gitlab.com/tud-mst/ptvpy>`_ |
`Documentation <https://tud-mst.gitlab.io/ptvpy>`_ |
`Issue tracker <https://gitlab.com/tud-mst/ptvpy/issues>`_

A free and open-source command line tool and Python library for
`particle tracking velocimetry`_ enabling a flexible workflow with reproducible
and reliable results.
PtvPy depends on and extends functionality from the excellent library trackpy_ and
provides a periphery to visualize, analyze and export the data involved.

It is developed at the `Chair of Measurement & Sensor System Technique`_ at the TU
Dresden to facilitate data analysis.
It was eventually published in 2019 as free and open-source software.

.. _particle tracking velocimetry:
   https://en.wikipedia.org/wiki/Particle_tracking_velocimetry
.. _trackpy: https://soft-matter.github.io/trackpy/
.. _Chair of Measurement & Sensor System Technique:
   https://tu-dresden.de/ing/elektrotechnik/iee/mst


Installation
============

PtvPy is available for Python 3.7 and higher and can be installed from PyPI with::

    pip install ptvpy

If you are using conda, use::

    conda install -c conda-forge ptvpy

If you want to install from source or need more details checkout the
`installation guide`_.

.. _installation guide: https://tud-mst.gitlab.io/ptvpy/installation.html


Contributing
============
Your own ideas, feedback and changes to PtvPy are always welcome!
Feel free open a new issue_ to get in touch and check out the `contribution guide`_.

.. _issue: https://gitlab.com/tud-mst/ptvpy/issues/new?issue
.. _contribution guide: https://gitlab.com/tud-mst/ptvpy/blob/master/CONTRIBUTING.rst


License
=======
PtvPy is licensed under the BSD 3-clause "New or revised" License.
Parts of this project may be copyrighted or compatibly licensed by third parties
as stated in the `license file`_.

.. _license file: https://gitlab.com/tud-mst/ptvpy/blob/master/LICENSE.txt

Copyright (c) 2018-2022, PtvPy developers. All rights reserved.
