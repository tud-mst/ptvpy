================================
Welcome to PtvPy's documentation
================================

.. image:: _images/slideshow-1.svg
   :target: introduction.html#image-slideshow-1
   :width: 19%
.. image:: _images/vector.svg
   :target: introduction.html#image-vector
   :width: 19%
.. image:: _images/scatter2d.png
   :target: introduction.html#image-scatter2d
   :width: 19%
.. image:: _images/scatter3d.svg
   :target: introduction.html#image-scatter3d
   :width: 19%
.. image:: _images/heatmap.svg
   :width: 19%

PtvPy is a free and open-source command line tool and Python library for
`particle tracking velocimetry`_ enabling a flexible workflow with reproducible
and reliable results.
PtvPy depends on and extends functionality from the excellent library trackpy_ and
provides a periphery to visualize, analyze and export the data involved.

PtvPy is developed at the `Chair of Measurement & Sensor System Technique`_ at the TU
Dresden to facilitate data analysis.
It was eventually published in 2019 as free and open-source software.

.. _particle tracking velocimetry:
   https://en.wikipedia.org/wiki/Particle_tracking_velocimetry
.. _trackpy: https://soft-matter.github.io/trackpy/
.. _Chair of Measurement & Sensor System Technique:
   https://tu-dresden.de/ing/elektrotechnik/iee/mst

.. toctree::
   :caption: Guides
   :maxdepth: 2

   installation
   introduction
   faq
   contributing
   maintaining

.. toctree::
   :caption: Reference
   :maxdepth: 2

   _generated/profile
   cli
   _generated/api
   changelog


